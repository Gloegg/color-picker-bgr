unit uMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TfrmMain = class(TForm)
    imColor: TImage;
    edColor: TEdit;
    btCopy: TButton;
    pnSelected: TPanel;
    imHue: TImage;
    edHTML: TEdit;
    btCopyHTML: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lbSample: TLabel;
    imFontColor: TImage;
    imFontHue: TImage;
    Label6: TLabel;
    Label7: TLabel;
    edFontDelphi: TEdit;
    Button1: TButton;
    edFontHTML: TEdit;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure imColorMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure imColorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure tbHueChange(Sender: TObject);
    procedure imHueMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure imHueMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btCopyClick(Sender: TObject);
    procedure btCopyHTMLClick(Sender: TObject);
    procedure pnSelectedClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure imFontHueMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure imFontHueMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure imFontColorMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure imFontColorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }
    fHue, fFontHue: Integer;
    fX, fY: Integer;
    fFontX, fFontY : integer;
    procedure RefreshBitmap;
    procedure RefreshFontBitmap;
    procedure InitHue;
    procedure SelectPixel(X, Y: Integer);
    procedure SelectFontPixel(X, Y: Integer);
    procedure WMMove(var Message: TMessage); message WM_MOVE;
  public
    { Public-Deklarationen }
    procedure SetColor(aColor : TColor);
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  uHSV,
  math,
  ClipBrd,
  uPalette, uZoom;

procedure TfrmMain.btCopyClick(Sender: TObject);
begin
  Clipboard.AsText := edColor.Text;
end;

procedure TfrmMain.btCopyHTMLClick(Sender: TObject);
begin
  Clipboard.AsText := edHTML.Text;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  Clipboard.AsText := edFontDelphi.Text;
end;

procedure TfrmMain.Button2Click(Sender: TObject);
begin
  Clipboard.AsText := edFontHTML.Text;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  fX := 256;
  fY := 256;
  fHue := 0;
  InitHue;
  RefreshBitmap;
  RefreshFontBitmap;
end;

procedure TfrmMain.FormResize(Sender: TObject);
begin
  frmPalette.Top := Top;
  frmPalette.Left := Left + Width;
  frmPalette.Height := Height;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  frmPalette.Top := Top;
  frmPalette.Left := Left + Width;
  frmPalette.Height := Height;
end;

procedure TfrmMain.imColorMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if button = mbLeft then
  begin
    fX := X;
    fY := Y;
    SelectPixel(X, Y);
  end;
end;

procedure TfrmMain.imColorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    fX := X;
    fY := Y;
    SelectPixel(X, Y);
  end;
end;

procedure TfrmMain.imFontColorMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if button = mbLeft then
  begin
    fFontX := X;
    fFontY := Y;
    SelectFontPixel(X, Y);
  end;
end;

procedure TfrmMain.imFontColorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    fFontX := X;
    fFontY := Y;
    SelectFontPixel(X, Y);
  end;
end;

procedure TfrmMain.imFontHueMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  fFontHue := X;
  fFontHue := min(359, fFontHue);
  fFontHue := max(0, fFontHue);
  RefreshFontBitmap;
  SelectFontPixel(fFontX, fFontY);
end;

procedure TfrmMain.imFontHueMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    fFontHue := X;
    fFontHue := min(359, fFontHue);
    fFontHue := max(0, fFontHue);
    RefreshFontBitmap;
    SelectFontPixel(fX, fY);
  end;
end;

procedure TfrmMain.imHueMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  fHue := X;
  fHue := min(359, fHue);
  fHue := max(0, fHue);
  RefreshBitmap;
  SelectPixel(fX, fY);
end;

procedure TfrmMain.imHueMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    fHue := X;
    fHue := min(359, fHue);
    fHue := max(0, fHue);
    RefreshBitmap;
    SelectPixel(fX, fY);
  end;
end;

procedure TfrmMain.InitHue;
var
  bmp: TBitmap;
  c: TColor;
  X: Integer;
begin
  bmp := TBitmap.Create;
  bmp.PixelFormat := pf32bit;
  bmp.SetSize(360, imHue.Height);

  for X := 0 to 359 do
  begin
    c := HSVtoRGB(X, 255, 255);
    bmp.Canvas.Pen.Color := c;
    bmp.Canvas.MoveTo(X, 0);
    bmp.Canvas.LineTo(X, imHue.Height - 1);
  end;

  imHue.Picture.Bitmap.Assign(bmp);
  imFontHue.Picture.Bitmap.Assign(bmp);
  bmp.Free;
end;

procedure TfrmMain.pnSelectedClick(Sender: TObject);
begin
  frmPalette.AddColor(pnSelected.Color);
  frmPalette.Show;
end;

procedure TfrmMain.RefreshBitmap;
type
  PPixArray = ^PixArray;
  PixArray = array [1 .. 3] of Byte;
var
  p: PPixArray;
  Y, X: Integer;
  c: TColor;
  bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  bmp.PixelFormat := pf24bit;
  bmp.SetSize(256, 256);

  for Y := 0 to 255 do
  begin
    p := bmp.ScanLine[255 - Y];
    for X := 0 to 255 do
    begin
      c := HSVtoRGB(fHue, Y, X);
      p[3] := c mod 256;
      c := c div 256;
      p[2] := c mod 256;
      c := c div 256;
      p[1] := c mod 256;
      inc(p);
    end;
  end;
  imColor.Picture.Bitmap.Assign(bmp);
  bmp.Free;
end;

procedure TfrmMain.RefreshFontBitmap;
type
  PPixArray = ^PixArray;
  PixArray = array [1 .. 3] of Byte;
var
  p: PPixArray;
  Y, X: Integer;
  c: TColor;
  bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  bmp.PixelFormat := pf24bit;
  bmp.SetSize(256, 256);

  for Y := 0 to 255 do
  begin
    p := bmp.ScanLine[255 - Y];
    for X := 0 to 255 do
    begin
      c := HSVtoRGB(fFontHue, Y, X);
      p[3] := c mod 256;
      c := c div 256;
      p[2] := c mod 256;
      c := c div 256;
      p[1] := c mod 256;
      inc(p);
    end;
  end;
  imFontColor.Picture.Bitmap.Assign(bmp);
  bmp.Free;
end;

procedure TfrmMain.SelectFontPixel(X, Y: Integer);
var
  c: TColor;
  r, g, b: Byte;
begin
  X := min(max(X, 0), 255);
  Y := min(max(Y, 0), 255);
  c := imFontColor.Canvas.Pixels[X, Y];
  r := c and $FF;
  g := (c shr 8) and $FF;
  b := (c shr 16) and $FF;
  edFontDelphi.Text := '$' + IntToHex(c, 6);
  edFontHTML.Text := '#' + IntToHex(r, 2) + IntToHex(g, 2) + IntToHex(b, 2);

  lbSample.Font.Color := c;
end;

procedure TfrmMain.SelectPixel(X, Y: Integer);
var
  c: TColor;
  r, g, b: Byte;
begin
  X := min(max(X, 0), 255);
  Y := min(max(Y, 0), 255);
  c := imColor.Canvas.Pixels[X, Y];
  edColor.Text := '$' + IntToHex(c, 6);
  r := c and $FF;
  g := (c shr 8) and $FF;
  b := (c shr 16) and $FF;
  edHTML.Text := '#' + IntToHex(r, 2) + IntToHex(g, 2) + IntToHex(b, 2);
  pnSelected.Color := c;
  pnSelected.Repaint;
end;

procedure TfrmMain.SetColor(aColor: TColor);
var
  r, g, b: Byte;
begin
  edColor.Text := '$' + IntToHex(aColor, 6);
  r := aColor and $FF;
  g := (aColor shr 8) and $FF;
  b := (aColor shr 16) and $FF;
  edHTML.Text := '#' + IntToHex(r, 2) + IntToHex(g, 2) + IntToHex(b, 2);
  pnSelected.Color := aColor;
  pnSelected.Repaint;
end;

procedure TfrmMain.tbHueChange(Sender: TObject);
begin
  RefreshBitmap;
end;

procedure TfrmMain.WMMove(var Message: TMessage);
begin
  if Assigned(frmPalette) then
  begin
    frmPalette.Top := Top;
    frmPalette.Left := Left + Width;
    frmPalette.Height := Height;
  end;
end;

end.
