unit uPalette;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.StdCtrls;

type
  TfrmPalette = class(TForm)
    fpPalette: TFlowPanel;
    Label1: TLabel;
    procedure OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure AddColor(aColor: TColor);
  end;

var
  frmPalette: TfrmPalette;

implementation

{$R *.dfm}

uses
  ClipBrd,
  uMain;

{ TfrmPalette }

procedure TfrmPalette.AddColor(aColor: TColor);
var
  pn: TPanel;
begin
  pn := TPanel.Create(self);
  pn.Height := 45;
  pn.Width := 45;
  pn.Color := aColor;
  pn.Parent := fpPalette;
  pn.ParentColor := false;
  pn.ParentCtl3D := false;
  pn.ParentBackground := false;
  pn.Repaint;
  pn.Margins.Left := 5;
  pn.Margins.Top := 5;
  pn.Margins.Right := 5;
  pn.Margins.Bottom := 5;
  pn.AlignWithMargins := true;
  pn.Cursor := crHandPoint;
  pn.OnMouseDown := OnMouseDown;
end;

procedure TfrmPalette.OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  pn: TPanel;
begin
  pn := Sender as TPanel;
  case Button of
    TMouseButton.mbLeft:
      frmMain.SetColor(pn.Color); // Clipboard.AsText := '$' + IntToHex(pn.Color,6);
    TMouseButton.mbRight:
      pn.Free;
  end;
end;

end.
