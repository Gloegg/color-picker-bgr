program BGR;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uHSV in 'uHSV.pas',
  uPalette in 'uPalette.pas' {frmPalette},
  uZoom in 'uZoom.pas' {frmZoom};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmPalette, frmPalette);
  Application.CreateForm(TfrmZoom, frmZoom);
  Application.Run;
end.
