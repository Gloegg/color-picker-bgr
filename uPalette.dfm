object frmPalette: TfrmPalette
  Left = 0
  Top = 0
  Hint = 
    'Left Click to select; Right Click to delete|Left Click to select' +
    '; Right Click to delete'
  BorderStyle = bsSizeToolWin
  Caption = 'Palette'
  ClientHeight = 366
  ClientWidth = 237
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 237
    Height = 13
    Align = alTop
    Caption = 'Left Click to select; Right Click to delete'
    ExplicitWidth = 189
  end
  object fpPalette: TFlowPanel
    Left = 0
    Top = 13
    Width = 237
    Height = 353
    Align = alClient
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    ExplicitTop = 0
    ExplicitHeight = 366
  end
end
