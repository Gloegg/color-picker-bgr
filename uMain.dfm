object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Color Picker'
  ClientHeight = 362
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imColor: TImage
    Left = 11
    Top = 41
    Width = 256
    Height = 256
    Cursor = crCross
    OnMouseDown = imColorMouseDown
    OnMouseMove = imColorMouseMove
  end
  object imHue: TImage
    Left = 8
    Top = 8
    Width = 360
    Height = 27
    Cursor = crCross
    OnMouseDown = imHueMouseDown
    OnMouseMove = imHueMouseMove
  end
  object Label1: TLabel
    Left = 30
    Top = 306
    Width = 29
    Height = 13
    Caption = 'Delphi'
  end
  object Label2: TLabel
    Left = 33
    Top = 337
    Width = 26
    Height = 13
    Caption = 'HTML'
  end
  object imFontColor: TImage
    Left = 377
    Top = 41
    Width = 256
    Height = 256
    Cursor = crCross
    OnMouseDown = imFontColorMouseDown
    OnMouseMove = imFontColorMouseMove
  end
  object imFontHue: TImage
    Left = 377
    Top = 8
    Width = 360
    Height = 27
    Cursor = crCross
    OnMouseDown = imFontHueMouseDown
    OnMouseMove = imFontHueMouseMove
  end
  object Label6: TLabel
    Left = 398
    Top = 309
    Width = 29
    Height = 13
    Caption = 'Delphi'
  end
  object Label7: TLabel
    Left = 401
    Top = 340
    Width = 26
    Height = 13
    Caption = 'HTML'
  end
  object edColor: TEdit
    Left = 67
    Top = 303
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '$ffffff'
  end
  object btCopy: TButton
    Left = 194
    Top = 301
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 1
    OnClick = btCopyClick
  end
  object pnSelected: TPanel
    Left = 273
    Top = 41
    Width = 98
    Height = 256
    Cursor = crHandPoint
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 2
    OnClick = pnSelectedClick
    object Label3: TLabel
      Left = 16
      Top = 32
      Width = 61
      Height = 13
      Caption = 'Lorem Ipsum'
    end
    object Label4: TLabel
      Left = 16
      Top = 64
      Width = 61
      Height = 13
      Caption = 'Lorem Ipsum'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 96
      Width = 61
      Height = 13
      Caption = 'Lorem Ipsum'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lbSample: TLabel
      Left = 16
      Top = 128
      Width = 61
      Height = 13
      Caption = 'Lorem Ipsum'
    end
  end
  object edHTML: TEdit
    Left = 67
    Top = 334
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '$ffffff'
  end
  object btCopyHTML: TButton
    Left = 194
    Top = 332
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 4
    OnClick = btCopyHTMLClick
  end
  object edFontDelphi: TEdit
    Left = 435
    Top = 306
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '$ffffff'
  end
  object Button1: TButton
    Left = 562
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 6
    OnClick = Button1Click
  end
  object edFontHTML: TEdit
    Left = 435
    Top = 336
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '$ffffff'
  end
  object Button2: TButton
    Left = 562
    Top = 332
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 8
    OnClick = Button2Click
  end
end
