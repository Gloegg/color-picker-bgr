unit uZoom;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids;

type
  TfrmZoom = class(TForm)
    grZoom: TDrawGrid;
    procedure grZoomDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
    fBitmap : TBitmap;
  public
    { Public-Deklarationen }
    procedure UpdateBMP(aBMP : TBitmap);
  end;

var
  frmZoom: TfrmZoom;

implementation

{$R *.dfm}

uses
  uHSV;

{ TForm1 }

{ TfrmZoom }

procedure TfrmZoom.FormCreate(Sender: TObject);
begin
  fBitmap := TBitmap.Create;
  fBitmap.SetSize(21,21);
end;

procedure TfrmZoom.FormDestroy(Sender: TObject);
begin
  fBitmap.Free;
end;

procedure TfrmZoom.grZoomDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  grZoom.Brush.Color := fBitmap.Canvas.Pixels[aCol, aRow];
  grZoom.Brush.Color := clBlue;
  grZoom.Canvas.FillRect(Rect);
end;

procedure TfrmZoom.UpdateBMP(aBMP: TBitmap);
var
  i: Integer;
  j: Integer;
begin
  fBitmap.Assign(aBMP);
  grZoom.Invalidate;
  for i := 0 to 20 do
  begin
    for j := 0 to 20 do
    begin
      grZoom.Canvas.Brush.Color := aBMP.Canvas.Pixels[i,j];
      grZoom.Canvas.FillRect(grZoom.CellRect(i,j));
    end;
  end;
end;

end.
